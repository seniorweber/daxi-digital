new WOW().init();

$('.carousel').carousel({
    interval: false
});

lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true
})

$(document).ready(function () {

    $(".menu-toggle").click(function() {
        $(".sandwich").toggleClass("active");
    });

    $(".main-menu__link").on("click", function() {
        $(".main-menu").fadeOut(600);
        $(".sandwich").toggleClass("active");
    });

    $(".menu-toggle").on("click", function() {
        $(".main-menu").fadeToggle(600);
    });

    $(".main-menu__link").mPageScroll2id();
    $(".button-up").mPageScroll2id();

    $('.faqs').parallax({imageSrc: 'images/parallax.jpg'});

    
    $('.faqs__answer').hide();

    $('.faqs__question').on('click', function () {

        var hiddenBlock = $('.faqs__answer');
        var index = $('.faqs__question').index(this);
        for (var i = 0; i < hiddenBlock.length; i++){
            if($(hiddenBlock[i]).is(":visible") && i != index){
                $(hiddenBlock[i]).hide(600);
                $(hiddenBlock[i]).prev().css("background", 'url("images/plus-ico.png") no-repeat 100% 40%');
            }
        }

        if ($(this).next().is(":visible")) {
            $(this).css("background", 'url("images/plus-ico.png") no-repeat 100% 40%');
        }else{
            $(this).css("background", 'url("images/minus-ico.png") no-repeat 100% 40%');
        }
        $(this).next().slideToggle(600);


    });

    $(".blog__box")
        .mouseover(function(){
            var src = $(this).children('img').attr("src");
            var new_src = src.replace(".png","-hover.png");
            $(this).children('img').attr("src",new_src);
        })
        .mouseout(function(){
            var src = $(this).children('img').attr("src");
            var new_src = src.replace("-hover.png",".png");
            $(this).children('img').attr("src",new_src);
        });

    
    $(".form-start").validate({
        rules: {
            phone: {
                required: true
            },
    
            name: {
                required: true
            },

            mail: {
                required: true
            }

        },
    
        
    });

    $("#phone").mask("+1 999 999 9999");

});


