'use strict';
var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    browserSync  = require('browser-sync'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglifyjs'),
    cssnano      = require('gulp-cssnano'),
    rename       = require('gulp-rename'),
    del          = require('del'),
    imagemin     = require('gulp-imagemin'),
    pngquant     = require('imagemin-pngquant'),
    cache        = require('gulp-cache'),
    autoprefixer = require('gulp-autoprefixer');
gulp.task('sass', function () {
    return gulp.src('src/sass/**/*.sass')
        .pipe(sass())
        .pipe(autoprefixer(['last 15 version', '> 1%', 'ie 8'], {cascade : true}))
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.reload({stream : true}));
});
gulp.task('scripts', function () {
   return gulp.src([
       'src/libs/jquery/dist/jquery.min.js',
       'src/libs/magnific-popup/dist/jquery.magnific-popup.min.js',
       'src/libs/bootstrap/dist/js/bootstrap.min.js',
       'src/libs/wow/dist/wow.min.js',
       'src/libs/parallax.js/parallax.min.js',
       'src/libs/lightbox2/dist/js/lightbox.min.js',
       'src/libs/jquery-validation/dist/jquery.validate.min.js',
       'src/libs/jquery.maskedinput/dist/jquery.maskedinput.min.js',
       'src/libs/jquery-validation/dist/jquery.validate.min.js',
       'src/js/components/PageScroll2id/PageScroll2id.min.js'

   ])
       .pipe(concat('libs.min.js'))
       .pipe(uglify())
       .pipe(gulp.dest('src/js'));
});
gulp.task('libs-css', ['sass'], function () {
    return gulp.src('src/css/libs.css')
        .pipe(cssnano())
        .pipe(rename({suffix : '.min'}))
        .pipe(gulp.dest('src/css'));
});
gulp.task('browser-sync', function () {
   browserSync({
       server:{
           baseDir :'src'
       },
       notify : false
   });
});
gulp.task('images', function () {
   return gulp.src('src/images/**/*')
       .pipe(cache(imagemin({
           interlaced : true,
           progressive : true,
           svgoPlugins : [{removeViewBox : false}],
           use : [pngquant()]
       })))
       .pipe(gulp.dest('dist/images'));
});
gulp.task('watch', ['browser-sync', 'libs-css', 'scripts'], function () {
    gulp.watch('src/sass/**/*.sass', ['sass']);
    gulp.watch('src/*.html', browserSync.reload);
    gulp.watch('src/js/**/*.js', browserSync.reload);

});
gulp.task('clean', function () {
   return del.sync('dist');
});
gulp.task('clear', function () {
    return cache.clearAll();
});
gulp.task('build',['clean', 'images', 'sass', 'scripts'], function () {
   var buildCss = gulp.src([
       'src/css/main.css',
       'src/css/libs.min.css'
   ])
       .pipe(gulp.dest('dist/css'));
   var buildComponentsCss = gulp.src([,
       'src/css/components/**/*'
   ])
       .pipe(gulp.dest('dist/css/components'));
    var buildVideo = gulp.src([,
        'src/video/**/*'
    ])
        .pipe(gulp.dest('dist/video/'));
   var buildFonts = gulp.src('src/fonts/**/*')
       .pipe(gulp.dest('dist/fonts'));
   var buildJs = gulp.src('src/js/**/*.js')
       .pipe(gulp.dest('dist/js'));
   var buildHtml = gulp.src('src/*.html')
       .pipe(gulp.dest('dist'));
});